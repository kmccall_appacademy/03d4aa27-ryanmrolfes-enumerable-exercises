require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.length.zero?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  ans = false
  long_strings.each { |string| ans = string.include?(substring) }
  ans
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  ans = []
  re_string = ''
  string.each_char do |letter|
    ans << letter if re_string.include?(letter) && ans.include?(letter) == false && letter != ' '
    re_string << letter
  end
  ans.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  longest = ''
  second = ''
  string.split.each do |word|
    if word.length > longest.length
      second = longest
      longest = word
    end
  end
  [second, longest]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').reject { |letter| string.include?(letter) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  ans = []
  (first_yr..last_yr).each { |year| ans << year if not_repeat_year?(year) }
  ans
end

def not_repeat_year?(year)
  return true if year.to_s.chars == year.to_s.chars.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  repeat = []
  songs.each_with_index do |song, i|
    repeat << song if songs[i] == songs[i + 1]
  end

  ans = []
  songs.each do |song|
    ans << song if repeat.include?(song) == false && ans.include?(song) == false
  end
  ans
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.delete!('.,?!')
  c_words = string.split.select { |word| word.include?('c') }
  ans = ''
  c_words.each_with_index do |word, idx|
    ans = word if idx.zero?
    ans = word if c_distance(word) < c_distance(ans)
  end
  ans
end

def c_distance(word)
  idx = word.length - 1
  while idx >= 0
    return idx if word[idx] == 'c' || word[idx] == 'C'
    idx -= 1
  end
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

# def repeated_number_ranges(arr)
  # start_idx = 0
  # end_idx = 0
  # ans = []
  # arr.each_with_index do |num, idx|
  #   if
  #     next if num == arr[idx - 1] && num == arr[idx + 1]
  #     start_idx = idx if num == arr[idx + 1]
  #     end_idx = idx if num == arr[idx - 1]
  #     ans << [start_idx, end_idx]
  # end
  # ans << [start_idx, end_idx]
  # ans
# end

def repeated_number_ranges(arr)
  ans = []
  start_idx = nil

  arr.each_with_index do |num, idx|
    if num == arr[idx + 1]
      start_idx = idx unless start_idx
    elsif start_idx
      ans << [start_idx, idx]
      start_idx = nil
    end
  end
  ans
end
